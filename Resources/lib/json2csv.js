/**
 * Main function, convert json to csv
 * 
 * @param {Object} params Containing data, fields, fieldNames
 * @param {Function} callback(err, csv) - Callback function
 * 	if error, return error in callback
 *  if success, return csv output in callback
 */
module.exports = function(params, callback){
	checkParams(params, function(err){
		if (err) return callback(err);
		createTitles(params, function(err, titles){
			if (err) return callback(err);
			createContent(params, titles, function (csv){
				callback(null, csv);
			});
		});
	});
};


/**
 * Local variables
 */
// Possibility to change delimiter
var del = ',';
// Line break
var lb = '\n';


/**
 * Get type of variable
 * @see http://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/
 * 
 * @param {Object} any variable to get type
 * @return {String} name of data type 
 */
function toType(obj) {
	return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
}


/**
 * Check passing params
 * 
 * @param {Object} params Parameters: data, fields and fieldNames
 * @param {Function} callback Callback function returning error if invalid fields
 */
function checkParams(params, callback){
	params.data = JSON.parse(JSON.stringify(params.data));
	
	// if params.data not an array but an object, create an array with 1 item
	if (toType(params.data) != 'array'){
		var a = new Array();
		a[0] = params.data;
		params.data = a;
	}
	
	params.fieldNames = params.fieldNames || params.fields;
	
	// Compare fieldNames and fields
	if (params.fieldNames && params.fieldNames.length !== params.fields.length){
		callback('fieldNames and fields should be of the same length, if fieldNames is provided');
	}
	
	callback(null);
}


/**
 * Create a title row for csv
 * 
 * @param {Object} params Paramters: data, fields and fieldNames
 * @param {Function} callback 
 */
function createTitles(params, callback){
	var str = '';
	
	params.fieldNames.forEach(function(element) {
		if (str !== ''){
			str += del;
		}
		str += JSON.stringify(element);
	});
	callback(null, str);
}


/**
 * Create content rows for csv
 * 
 * @param {Object} params Parameters: data, fields, fieldNames
 * @param {String} str Title row as a string
 * @param {Function} callback
 */
function createContent(params, str, callback){
	params.data.forEach(function(obj){
		// If null or empty, do nothing
		if (obj && Object.getOwnPropertyNames(obj).length > 0){
			
			var line = '';
			params.fields.forEach(function(item){
				if (obj.hasOwnProperty(item)){
					line += JSON.stringify(obj[item]);
				}
				line += del;
			});
			
			//remove last delimeter
			line = line.substring(0, line.length-1);
			line = line.replace(/\\"/g, '""');
			str += lb + line;
		}
	});
	callback(str);
}
