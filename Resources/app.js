// Temporary data for example project
// Replace this with real data
var file = Ti.Filesystem.getFile('testData.json');
var data = file.read().text;
var jsonArray = JSON.parse(data).results; 


/**
 * Open Email Dialog with CSV attached 
 * 
 * @param {String} csv The exported CSV data, as a string
 */
function sendEmailWithAttachment(csv){
	// Create csv file 
	var fileName = 'data'; // Name of file. Can be change but use lowercase a-z 
	var file  = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, fileName+'.csv');
	file.write(csv);
	
	if (!file.exists){
		// Failed to create csv file
		//TODO: Show some Error Alert for user
		console.log('Failed to create csv file');
	} else {
		
		// Create email
		// TODO: Change subject and messageBody
		var emailDialog = Ti.UI.createEmailDialog({
			subject: 'EMAIL SUBJECT GOES HERE',
			messageBody: 'BODY MESSAGE GOES HERE',
		});	
		
		emailDialog.addAttachment(file);
		emailDialog.open();
	}
	
}


/**
 * This is were the CSV magic happens!
 */
var json2csv = require('lib/json2csv');
function exportToCsv(){
	
	/**
	 * Parameter object for json2csv
	 * 
	 * @param {Object} JSON data
	 * @param {Array} fields - Properties in JSON data used to create CSV data
	 * @param {Array} fieldNames - Header Titles for columns in CSV. Optional, if null or not existing it uses fields-properties as header titles
	 * 
	 * Note: Order of fields and fieldNames must match. The order of fields/fieldNames will be reflected in the csv-file.
	 */
	var obj = {
		data: jsonArray, // JSON data
		fields: ['fullTank', 'price', 'fordon', 'liter', 'signatur', 'timStatus', 'anteckning', 'year', 'fordonId', 'arbetsmoment', 'medelForbrukning', 'month', 'date', 'createdAt', 'updatedAt'],
		fieldNames: ['fullTank', 'price', 'fordon', 'liter', 'signatur', 'timStatus', 'anteckning', 'year', 'fordonId', 'arbetsmoment', 'medelForbrukning', 'month', 'date', 'createdAt', 'updatedAt']
	};
	
	// Create csv
	json2csv(obj, function(err, csv){
		if (err){
			// Failed to create csv
			//TODO: Show some Error Alert for user
			console.log(err);
		} else {
			console.log('CSV SUCCESS \n\n' + csv);
			sendEmailWithAttachment(csv);
				
		}
	});
}


/**
 * Basic window setup
 */
var win = Ti.UI.createWindow({
	backgroundColor: '#fff',
	width: Ti.UI.FILL,
	height: Ti.UI.FILL
});

var btn = Ti.UI.createButton({
	title: 'Press Me!',
	height: Ti.UI.SIZE,
	width: Ti.UI.SIZE
});
btn.addEventListener('click', exportToCsv);
win.add(btn);


win.open();